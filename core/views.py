from django.http import HttpResponse
from .models import *
from django.shortcuts import get_object_or_404, redirect, render
from django.shortcuts import render
from .models import Canal
import requests 
import codecs


# Create your views here.
def show(request,pk):
    row = get_object_or_404(List, user=pk)
    return redirect(row.url)

def baixar_arquivo(url, endereco=None):
    if endereco is None:
        endereco = os.path.basename(url.split("?")[0])
    resposta = requests.get(url, stream=True)
    if resposta.status_code == requests.codes.OK:
        with open(endereco, 'wb') as novo_arquivo:
            for parte in resposta.iter_content(chunk_size=256):
                novo_arquivo.write(parte)
        print("Download finalizado. Arquivo salvo em: {}".format(endereco))
    else:
        resposta.raise_for_status()


def list_external(request,pk):
       request.encoding = 'utf-8'
       row = get_object_or_404(List, user=pk)
       test_url = row.url
       baixar_arquivo(test_url,'resultado/lista.m3u')
       arq = codecs.open('resultado/lista.m3u', 'r', encoding='utf-8')
       texto = arq.readlines()
       dados = ''
       for linha in texto :
              if linha.strip()!='':
                     dados += linha
       arq.close()
       response = HttpResponse(dados, content_type="application/m3u")
       response['Content-Disposition'] = 'attachment; filename="list.m3u"'
       return response


def generate_m3u(request):
    linha = '#EXTM3U\n'
    for canal in Canal.objects.filter(status=200).order_by('name'):
        linha += '#EXTINF:{}, tvg-id="{} - {}" tvg-name="{} - {}" tvg-logo="{}" group-title="{}",{}\n{}\n'.format(canal.id, canal.id, canal.name,
                                                                                                                   canal.name,
                                                                                                            canal.id,

                                                                                                                   canal.logo,
                                                                                                                   canal.group_title,
                                                                                                                   canal.name,
                                                                                                                   canal.uri)
    dados = linha                                                                                                               
    return HttpResponse(dados, content_type="application/m3u")


def generate_m3ux(request):
    f = open("resultado/lista.m3u", "a")
    f.write("#EXTM3U\n")
    for canal in Canal.objects.filter(status=200).order_by('name'):
        f.write('#EXTINF:{}, tvg-id="{} - {}" tvg-name="{} - {}" tvg-logo="{}" group-title="{}",{}\n{}\n'.format(canal.id, canal.id, canal.name,
                                                                                                                   canal.name,
                                                                                                            canal.id,

                                                                                                                   canal.logo,
                                                                                                                   canal.group_title,
                                                                                                                   canal.name,
                                                                                                                   canal.uri))

    fsock = open("resultado/lista.m3u", "rb")
    return HttpResponse(fsock, content_type='text')